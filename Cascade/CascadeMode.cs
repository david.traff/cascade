﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade
{
    public enum CascadeMode
    {
        NoAction,
        Cascade,
        SetNull,
        Restrict
    }
}
