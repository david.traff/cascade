﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Configuration
{
    public interface ICascadeConfiguration
    {
        IPrimaryKeyMapper PrimaryKeyMapper { get; }

        IReadOnlyCollection<Type> Types { get; }

        IDataStore Store { get; }

        IEntityEventHandler EntityEventHandler { get; }
    }
}
