﻿using Cascade.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Configuration
{
    public class CascadeCoreBuilder : ICascadeConfiguration
    {
        #region Registration
        public static CascadeCoreBuilder Create() => new CascadeCoreBuilder();

        private bool _IsRegistered = false;

        private void EnsureUnregistered()
        {
            if (_IsRegistered)
                throw new InvalidOperationException($"Cannot modify an instance of {nameof(CascadeCoreBuilder)} if already registered.");
        }

        public CascadeCore Register()
        {
            EnsureUnregistered();

            CascadeCore.Register(this);

            return CascadeCore.Instance;
        }
        #endregion

        private CascadeCoreBuilder()
        {
            PrimaryKeyMapper = new DefaultPrimaryKeyMapper();
            EntityEventHandler = new IntegrityPass(new DefaultEntityEventHandler());
        }

        public IPrimaryKeyMapper PrimaryKeyMapper { get; private set; }

        private HashSet<Type> _Types; // Null means we want to automatically scan for types.
        public IReadOnlyCollection<Type> Types => _Types;

        public IDataStore Store { get; private set; }

        public IEntityEventHandler EntityEventHandler { get; private set; }

        public CascadeCoreBuilder WithPrimaryKeyMapper<TMapper>(TMapper instance) where TMapper : IPrimaryKeyMapper
        {
            EnsureUnregistered();

            PrimaryKeyMapper = instance;

            return this;
        }

        public CascadeCoreBuilder WithPrimaryKeyMapper<TMapper>() where TMapper : IPrimaryKeyMapper => WithPrimaryKeyMapper(Activator.CreateInstance<TMapper>());

        public CascadeCoreBuilder WithTypes(params Type[] types)
        {
            EnsureUnregistered();

            if (_Types == null)
                _Types = new HashSet<Type>();

            foreach (var type in types)
            {
                if (!typeof(ICascadable).IsAssignableFrom(type))
                    throw new ArgumentException($"The type {type.FullName} does not implement {nameof(ICascadable)}");

                _Types.Add(type);
            }

            return this;
        }

        public CascadeCoreBuilder WithType<TType>() where TType : ICascadable => WithTypes(typeof(TType));

        public CascadeCoreBuilder WithStore<TDataStore>(TDataStore instance) where TDataStore : IDataStore
        {
            EnsureUnregistered();

            Store = instance;

            return this;
        }

        public CascadeCoreBuilder WithEventHandler<TEventHandler>(TEventHandler instance) where TEventHandler : IEntityEventHandler
        {
            EnsureUnregistered();

            EntityEventHandler = instance;

            return this;
        }

        public CascadeCoreBuilder WithEventHandler<TEventHandler>() where TEventHandler : IEntityEventHandler => WithEventHandler(Activator.CreateInstance<TEventHandler>());
    }
}
