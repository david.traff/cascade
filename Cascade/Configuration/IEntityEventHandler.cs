﻿using Cascade.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Configuration
{
    public interface IEntityEventHandler
    {
        MutableResult DeleteEntity(EntityContext context);

        MutableResult UpdateEntity(EntityContext context);

        IntegrityResult CheckIntegrity(EntityContext context);
    }
}
