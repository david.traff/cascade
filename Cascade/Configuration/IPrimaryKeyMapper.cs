﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Cascade.Configuration
{
    public interface IPrimaryKeyMapper
    {
        PropertyInfo GetPrimaryKey(Type type);
    }
}
