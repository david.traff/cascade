﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class PrimaryKeyAttribute : Attribute
    {
    }
}
