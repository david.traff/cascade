﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class ForeignKeyAttribute : Attribute
    {
        public ForeignKeyAttribute(Type parent, CascadeMode update = CascadeMode.NoAction, CascadeMode delete = CascadeMode.NoAction)
        {
            Parent = parent;
            OnUpdate = update;
            OnDelete = delete;
        }

        public Type Parent { get; }

        public CascadeMode OnUpdate { get; }

        public CascadeMode OnDelete { get; }
    }
}
