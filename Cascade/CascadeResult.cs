﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade
{
    public class CascadeResult
    {
        public CascadeResult(bool success)
        {
            Success = success;
        }

        /// <summary>
        /// Tells weather this operation was successful.
        /// </summary>
        public bool Success { get; }
    }
}
