﻿using Cascade.Attributes;
using Cascade.Configuration;
using Cascade.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Cascade
{
    class Util
    {
        public static Func<object, Predicate<ICascadable>> CreateLambda<TCollection>(PropertyInfo property) where TCollection : ICascadable
        {
            //  (System.Object objvalue) =>
            //  {
            //      var value = (property.PropertyType)objvalue;
            //      Predicate<ICascadable> predicate_Cascadable = itemvalue =>
            //      {
            //          var item = (TCollection)itemvalue;
            //          item.[Property] == value;
            //      };

            //      return predicate_Cascadable;
            //  }

            var objvalueParam = Expression.Parameter(typeof(object), "objvalue");
            var valueVar = Expression.Variable(property.PropertyType, "value");

            var predicate = CreatePredicate<TCollection>(valueVar, property);
            var predicateVar = Expression.Variable(typeof(Predicate<ICascadable>));

            var block = Expression.Block(
                new ParameterExpression[] { valueVar, predicateVar },
                new Expression[]
                {
                    Expression.Assign(
                        valueVar,
                        Expression.Convert(
                            objvalueParam,
                            property.PropertyType
                        )
                    ),
                    Expression.Assign(
                        predicateVar,
                        predicate
                    ),
                    predicateVar //return this value.
                }
            );

            var lambda = Expression.Lambda<Func<object, Predicate<ICascadable>>>(block, objvalueParam);

            return lambda.Compile();
        }

        private static Expression<Predicate<ICascadable>> CreatePredicate<TCollection>(ParameterExpression value, PropertyInfo property) where TCollection : ICascadable
        {
            var itemParam = Expression.Parameter(typeof(ICascadable), "itemvalue");
            var itemVar = Expression.Variable(typeof(TCollection), "item");

            var block = Expression.Block(
                new ParameterExpression[] { itemVar },
                new Expression[]
                {
                    Expression.Assign(
                        itemVar,
                        Expression.Convert(
                            itemParam,
                            typeof(TCollection)
                        )
                    ),
                    Expression.Equal(
                        Expression.Property(
                            itemVar,
                            property
                        ),
                        value
                    )
                }
            );

            return Expression.Lambda<Predicate<ICascadable>>(block, itemParam);
        }

        public static object GetDefault(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }

        public static IEnumerable<Type> ScanCascadables()
        {
            return AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(x => !x.FullName.Contains("TraceDataCollector")) //... for testing
                .SelectMany(x => x.GetTypes())
                .Where(x => typeof(ICascadable).IsAssignableFrom(x) && x != typeof(ICascadable));
        }

        public static bool IsHashSet(Type t)
        {
            if (t.IsGenericType)
                return t.GetGenericTypeDefinition() == typeof(HashSet<>);

            return false;
        }
    }
}
