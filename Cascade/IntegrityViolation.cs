﻿using Cascade.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade
{
    public class IntegrityViolation
    {
        public IntegrityViolation(ForeignKey constraint, CascadeAttributes parent, object parentInstance, CascadeAttributes child, object childInstance)
        {
            Constraint = constraint ?? throw new ArgumentException(nameof(constraint));

            ParentAttributes = parent ?? throw new ArgumentException(nameof(parent));
            ParentInstance = parentInstance ?? throw new ArgumentException(nameof(parentInstance));

            ChildAttributes = child ?? throw new ArgumentException(nameof(child));
            ChildInstance = childInstance ?? throw new ArgumentException(nameof(childInstance));
        }

        public ForeignKey Constraint { get; }

        public CascadeAttributes ParentAttributes { get; }
        public object ParentInstance { get; }
        public object ParentKey => ParentAttributes.Accessor[ParentInstance, ParentAttributes.PrimaryKey.Property.Name];

        public CascadeAttributes ChildAttributes { get; }
        public object ChildInstance { get; }
        public object ChildKey => ChildAttributes.Accessor[ChildInstance, ChildAttributes.PrimaryKey.Property.Name];

        public override string ToString()
        {
            return $"Integrity-violation between parent \"{ParentAttributes.Type.FullName}\" and \"{ChildAttributes.Type.FullName}\". Primary key value on the referenced entity is \"{ParentKey}\"";
        }
    }
}
