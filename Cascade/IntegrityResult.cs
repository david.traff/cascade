﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade
{
    public class IntegrityResult : CascadeResult
    {
        public IntegrityResult(IEnumerable<IntegrityViolation> violations) : base(violations.Count() == 0)
        {
            Violations = violations;
        }

        public IntegrityResult(params IntegrityViolation[] violations) : base (violations.Length == 0)
        {
            Violations = violations;
        }

        public IEnumerable<IntegrityViolation> Violations { get; }
    }
}
