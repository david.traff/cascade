﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade
{
    public class MutableResult : CascadeResult
    {
        public MutableResult(bool success, IEnumerable<object> entities, int count) : base(success)
        {
            AffectedEntities = entities ?? throw new ArgumentException(nameof(entities));
            Count = count;
        }

        /// <summary>
        /// Holds the entities which was affected by this action.
        /// </summary>
        public IEnumerable<object> AffectedEntities { get; }

        /// <summary>
        /// Holds the number of entities which was affected by this action.
        /// </summary>
        public int Count { get; }
    }
}
