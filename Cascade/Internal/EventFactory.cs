﻿using Cascade.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade.Internal
{
    public class EventFactory
    {
        public EventFactory(IEntityEventHandler handler)
        {
            _Handler = handler;
        }

        private readonly IEntityEventHandler _Handler;

        public MutableResult DeleteEntity(object entity)
        {
            var context = GetContext(entity, ContextType.Delete);

            if (context == null)
                return new MutableResult(false, Enumerable.Empty<object>(), 0);

            return _Handler.DeleteEntity(context);
        }

        public MutableResult UpdateEntity(object entity)
        {
            var context = GetContext(entity, ContextType.Update);

            if (context == null)
                return new MutableResult(false, Enumerable.Empty<object>(), 0);

            return _Handler.UpdateEntity(context);
        }

        public IntegrityResult CheckIntegrity(object entity)
        {
            var context = GetContext(entity, ContextType.Delete);

            if (context == null)
                return new IntegrityResult(Enumerable.Empty<IntegrityViolation>());

            return _Handler.CheckIntegrity(context);
        }

        private EntityContext GetContext(object entity, ContextType contextType)
        {
            if (entity == null)
                throw new ArgumentException("Entity cannot be null.");

            var type = entity.GetType();

            if (!CascadeCore.Instance.TryGetAttributes(type, out var attributes))
                throw new ArgumentException($"{nameof(entity)} must be registered to {nameof(CascadeCore)}");

            return new EntityContext(
                entity: entity,
                key: attributes.Accessor[entity, attributes.PrimaryKeyProperty.Name],
                attributes: attributes,
                contextType
            );
        }
    }
}
