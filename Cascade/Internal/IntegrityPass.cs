﻿using Cascade.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade.Internal
{
    public class IntegrityPass : IEntityEventHandler
    {
        public IntegrityPass(IEntityEventHandler next)
        {
            _Next = next ?? throw new ArgumentException(nameof(next));
        }

        private readonly IEntityEventHandler _Next;

        public MutableResult DeleteEntity(EntityContext context)
        {
            AssertIntegrity(context);

            return _Next.DeleteEntity(context);
        }

        public IntegrityResult CheckIntegrity(EntityContext context) => CheckIntegrity(context, false);

        public MutableResult UpdateEntity(EntityContext context)
        {
            foreach (var foreign in context.Attributes.ForeignKeys)
            {
                var value = context.Attributes.Accessor[context.Entity, foreign.Property.Name];
                var parent = GetParent(context, foreign);
                var exists = parent != null;

                if (exists && foreign.GetMode(context.ContextType) == CascadeMode.Restrict)
                {
                    if (!CascadeCore.Instance.TryGetAttributes(foreign.ParentType, out var attr))
                        throw new InvalidOperationException("Could not create IntegrityViolation.");

                    throw new EntityRestrictedException(new IntegrityViolation(foreign, attr, parent, context.Attributes, context.Entity));
                }

                if (!exists && value != GetDefault(foreign.Property.PropertyType))
                    throw new MissingParentException($"Invalid foreign-key value for {context.Attributes.Type.Name}: \"{context.Key}\". The parent doesn't exist.");
            }

            AssertIntegrity(context);

            return _Next.UpdateEntity(context);
        }

        private void AssertIntegrity(EntityContext context)
        {
            var result = CheckIntegrity(context, true);

            if (!result.Success)
                throw new EntityRestrictedException(result.Violations.First());
        }

        private IntegrityResult CheckIntegrity(EntityContext context, bool abortEarly)
        {
            var violations = new List<IntegrityViolation>();

            Traverse(context, violations, abortEarly);

            return new IntegrityResult(violations);
        }

        private void Traverse(EntityContext context, List<IntegrityViolation> violations, bool abortEarly)
        {
            foreach (var key in context.Attributes.ChildKeys)
            {
                var type = key.GetMode(context.ContextType);

                //If our child doesnt have an action here or if it only wants to set their referenced column to null we don't care
                //to check deeper within the integrity.
                if (type == CascadeMode.NoAction || type == CascadeMode.SetNull)
                    continue;

                if (!CascadeCore.Instance.TryGetAttributes(key.Type, out var attributes))
                    continue;

                var entities = key.Collection.Where(context.Key);

                foreach (var entity in entities)
                {
                    if (type == CascadeMode.Restrict)
                    {
                        violations.Add(new IntegrityViolation(key, context.Attributes, context.Entity, attributes, entity));

                        if (abortEarly)
                            return;
                    }

                    Traverse(new EntityContext(
                        entity,
                        attributes.Accessor[entity, attributes.PrimaryKeyProperty.Name],
                        attributes,
                        context.ContextType
                    ), violations, abortEarly);
                }
            }
        }

        private object GetParent(EntityContext context, ForeignKey key)
        {
            var keyValue = context.Attributes.Accessor[context.Entity, key.Property.Name];

            return key.ParentKey.Collection.Restrict(keyValue);
        }

        private object GetDefault(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }
    }
}
