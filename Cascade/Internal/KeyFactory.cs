﻿using Cascade.Configuration;
using FastMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Cascade.Internal
{
    public class KeyFactory : StorelessKeyFactory
    {
        public KeyFactory(IDataStore store, IPrimaryKeyMapper mapper, IEnumerable<Type> types) : base(mapper, types)
        {
            _Store = store ?? throw new ArgumentException(nameof(store));

            _StoreType = store.GetType();
            _Accessor = TypeAccessor.Create(_StoreType);

            _Properties = _StoreType
                .GetProperties()
                .Where(x => Util.IsHashSet(x.PropertyType));
        }

        private readonly IEnumerable<PropertyInfo> _Properties;
        private readonly IDataStore _Store;
        private readonly TypeAccessor _Accessor;
        private readonly Type _StoreType;

        public override IEnumerable<IKey> CreateKeys()
        {
            var keys = base.CreateKeys();
            var transformed = new List<IKey>();

            foreach (var primary in keys.OfType<PrimaryKey>())
                transformed.Add(new PrimaryKey(primary.Property, GetCollection(primary)));

            var primaries = transformed.OfType<PrimaryKey>();

            foreach (var foreign in keys.OfType<ForeignKey>())
                transformed.Add(new ForeignKey(foreign.Property, primaries.FirstOrDefault(x => x.Type == foreign.ParentType), GetCollection(foreign), foreign.OnUpdate, foreign.OnDelete));

            return transformed;
        }

        private ICollectionWrapper GetCollection(IKey key)
        {
            var collectionProperty = _Properties.FirstOrDefault(x => x.PropertyType.GenericTypeArguments[0] == key.Type);

            if (collectionProperty == null)
                return null;

            var collection = _Accessor[_Store, collectionProperty.Name];

            return new CollectionWrapper(collection, key.Property);
        }
    }
}
