﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Cascade.Internal
{
    public class ForeignKey : IKey
    {
        public ForeignKey(PropertyInfo property, PrimaryKey parent, ICollectionWrapper collection, CascadeMode update, CascadeMode delete)
        {
            OnUpdate = update;
            OnDelete = delete;
            Property = property;
            ParentKey = parent;
            Collection = collection;
        }

        public CascadeMode OnUpdate { get; }

        public CascadeMode OnDelete { get; }

        public PropertyInfo Property { get; }

        public Type Type => Property.ReflectedType;

        public ICollectionWrapper Collection { get; }

        public PropertyInfo ParentProperty => ParentKey.Property;

        public Type ParentType => ParentKey.Type;

        public PrimaryKey ParentKey { get; }

        public CascadeMode GetMode(ContextType type)
        {
            switch (type)
            {
                case ContextType.Delete:
                    return OnDelete;
                case ContextType.Update:
                    return OnUpdate;
            }

            throw new InvalidOperationException($"Cannot get {nameof(CascadeMode)} when {nameof(ContextType)} is {nameof(ContextType.None)}");
        }
    }
}
