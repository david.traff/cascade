﻿using System.Collections.Generic;

namespace Cascade.Internal
{
    public interface ICollectionWrapper
    {
        IEnumerable<object> RemoveWhere(object key);

        IEnumerable<object> SetNull(object key);

        object Restrict(object key);

        IEnumerable<object> Where(object key);
    }
}
