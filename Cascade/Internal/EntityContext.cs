﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Internal
{
    public class EntityContext
    {
        public EntityContext(object entity, object key, CascadeAttributes attributes, ContextType contextType)
        {
            Entity = entity;
            Key = key;
            Attributes = attributes;
            ContextType = contextType;
        }

        public object Entity { get; }

        public object Key { get; }

        public CascadeAttributes Attributes { get; }

        public ContextType ContextType { get; }
    }
}
