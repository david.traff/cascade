﻿using Cascade.Attributes;
using Cascade.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Cascade.Internal
{
    public class DefaultPrimaryKeyMapper : IPrimaryKeyMapper
    {
        public PropertyInfo GetPrimaryKey(Type type)
        {
            var properties = type.GetProperties();

            return properties.FirstOrDefault(x => x.GetCustomAttribute<PrimaryKeyAttribute>() != null);
        }
    }
}
