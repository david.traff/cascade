﻿using Cascade.Attributes;
using Cascade.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Cascade.Internal
{
    public class StorelessKeyFactory
    {
        public StorelessKeyFactory(IPrimaryKeyMapper mapper, IEnumerable<Type> types)
        {
            Mapper = mapper;
            Types = types;
        }

        protected IPrimaryKeyMapper Mapper { get; }
        protected IEnumerable<Type> Types { get; }

        public virtual IEnumerable<IKey> CreateKeys()
        {
            var keys = new List<IKey>();
            foreach(var type in Types)
            {
                var primary = Mapper.GetPrimaryKey(type);

                keys.Add(new PrimaryKey(primary, null));
            }

            foreach(var type in Types)
            {
                keys.AddRange(GetForeignKeys(type, keys.OfType<PrimaryKey>()));
            }

            return keys;
        }

        private IEnumerable<IKey> GetForeignKeys(Type type, IEnumerable<PrimaryKey> keys)
        {
            foreach (var property in type.GetProperties())
            {
                var attr = property.GetCustomAttribute<ForeignKeyAttribute>();

                if (attr == null)
                    continue;

                var parent = keys.FirstOrDefault(x => x.Type == attr.Parent);

                yield return new ForeignKey(property, parent, null, attr.OnUpdate, attr.OnDelete);
            }
        }
    }
}
