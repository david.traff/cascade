﻿using Cascade.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade.Internal
{
    public class DefaultEntityEventHandler : IEntityEventHandler
    {
        public MutableResult DeleteEntity(EntityContext context)
        {
            var affected = new List<object>();

            affected.AddRange(context.Attributes.PrimaryKey.Collection.RemoveWhere(context.Key));

            Delete(context, affected);

            return new MutableResult(true, affected, affected.Count);
        }

        private void Delete(EntityContext context, List<object> affected)
        {
            foreach (var foreign in context.Attributes.ChildKeys)
            {
                if (foreign.OnDelete == CascadeMode.SetNull)
                {
                    affected.AddRange(foreign.Collection.SetNull(context.Key));
                    continue;
                }
                else if (foreign.OnDelete != CascadeMode.Cascade)
                    continue;

                if (!CascadeCore.Instance.TryGetAttributes(foreign.Type, out var attributes))
                    continue;

                var deleted = foreign.Collection.RemoveWhere(context.Key);

                foreach (var item in deleted)
                {
                    affected.Add(item);

                    Delete(new EntityContext(
                        entity: item,
                        key: attributes.Accessor[item, attributes.PrimaryKey.Property.Name],
                        attributes,
                        ContextType.Delete
                    ), affected);
                }
            }
        }

        public IntegrityResult CheckIntegrity(EntityContext context)
        {
            foreach (var foreign in context.Attributes.ChildKeys)
            {
                var entity = foreign.Collection.Restrict(context.Key);
                if (!CascadeCore.Instance.TryGetAttributes(foreign.Type, out var attributes))
                    throw new InvalidOperationException("Could not create IntegrityViolation");

                if (entity != null)
                    return new IntegrityResult(new IntegrityViolation(foreign, context.Attributes, context.Entity, attributes, entity));
            }

            return new IntegrityResult();
        }

        public MutableResult UpdateEntity(EntityContext context)
        {
            var affected = new List<object>() { context.Entity };

            foreach(var foreign in context.Attributes.ChildKeys)
            {
                if (foreign.OnUpdate == CascadeMode.SetNull)
                    affected.AddRange(foreign.Collection.SetNull(context.Key));
            }

            return new MutableResult(true, affected, affected.Count);
        }
    }
}
