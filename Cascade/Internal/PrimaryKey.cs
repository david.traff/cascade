﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Cascade.Internal
{
    public class PrimaryKey : IKey
    {
        public PrimaryKey(PropertyInfo property, ICollectionWrapper collection)
        {
            Property = property;
            Collection = collection;
        }

        public PropertyInfo Property { get; }

        public Type Type => Property.ReflectedType;

        public ICollectionWrapper Collection { get; }
    }
}
