﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Internal
{
    public class EntityRestrictedException : Exception
    {
        public EntityRestrictedException(IntegrityViolation violation)
        {
            Violation = violation ?? throw new ArgumentException(nameof(violation));
        }

        public IntegrityViolation Violation { get; }

        public override string Message => Violation.ToString();
    }
}
