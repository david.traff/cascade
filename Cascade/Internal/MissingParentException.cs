﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Internal
{
    public class MissingParentException : Exception
    {
        public MissingParentException(string message) : base(message)
        {

        }
    }
}
