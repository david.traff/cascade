﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Cascade.Internal
{
    public interface IKey
    {
        Type Type { get; }

        PropertyInfo Property { get; }

        ICollectionWrapper Collection { get; }
    }
}
