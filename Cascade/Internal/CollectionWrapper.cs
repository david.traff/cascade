﻿using Cascade.Internal;
using FastMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Cascade.Internal
{
    public class CollectionWrapper<TCollection> : ICollectionWrapper where TCollection : ICascadable
    {
        public CollectionWrapper(HashSet<TCollection> collection, PropertyInfo property)
        {
            if (collection == null)
                throw new ArgumentException(nameof(collection));

            if (property == null || property.ReflectedType != typeof(TCollection))
                throw new ArgumentException(nameof(property));

            _Instance = collection;
            _Property = property;
            _PredicateBuilder = Util.CreateLambda<TCollection>(property);
            _Accessor = TypeAccessor.Create(typeof(TCollection));
        }

        private readonly HashSet<TCollection> _Instance;
        private readonly Func<object, Predicate<ICascadable>> _PredicateBuilder;
        private readonly TypeAccessor _Accessor;
        private readonly PropertyInfo _Property;

        public IEnumerable<object> RemoveWhere(object key)
        {
            var predicate = _PredicateBuilder(key);
            var items = new List<object>();

            foreach(var item in _Instance.Where((item) => predicate(item)))
                items.Add(item);

            _Instance.RemoveWhere((item) => predicate(item));

            return items;
        }

        public IEnumerable<object> SetNull(object key)
        {
            var predicate = _PredicateBuilder(key);
            var items = new List<object>();

            foreach (var item in _Instance.Where((item) => predicate(item)))
            {
                _Accessor[item, _Property.Name] = Util.GetDefault(_Property.PropertyType);

                items.Add(item);
            }

            return items;
        }

        public object Restrict(object key)
        {
            var predicate = _PredicateBuilder(key);

            return _Instance.FirstOrDefault((item) => predicate(item));
        }

        public IEnumerable<object> Where(object key)
        {
            var predicate = _PredicateBuilder(key);

            return _Instance.Where((item) => predicate(item)).Cast<object>();
        }
    }

    public class CollectionWrapper : ICollectionWrapper
    {
        public CollectionWrapper(object collection, PropertyInfo property)
        {
            var type = collection.GetType();

            if (!Util.IsHashSet(type))
                throw new ArgumentException(nameof(collection));

            var collectionType = type.GenericTypeArguments[0];
            var wrapperType = typeof(CollectionWrapper<>).MakeGenericType(collectionType);

            _WrapperInstance = Activator.CreateInstance(wrapperType, collection, property) as ICollectionWrapper;
        }

        private readonly ICollectionWrapper _WrapperInstance;

        public IEnumerable<object> RemoveWhere(object key) => _WrapperInstance.RemoveWhere(key);

        public object Restrict(object key) => _WrapperInstance.Restrict(key);

        public IEnumerable<object> SetNull(object key) => _WrapperInstance.SetNull(key);

        public IEnumerable<object> Where(object key) => _WrapperInstance.Where(key);
    }
}
