﻿using FastMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Cascade.Internal
{
    public class CascadeAttributes
    {
        public CascadeAttributes(Type type, PropertyInfo primaryKey, IEnumerable<IKey> keys)
        {
            Type = type ?? throw new ArgumentException(nameof(type));

            if (primaryKey == null || primaryKey.ReflectedType != type)
                throw new ArgumentException($"Primary key cannot be null on type {type.FullName}");

            PrimaryKeyProperty = primaryKey;
            Accessor = TypeAccessor.Create(type);
            Keys = keys ?? throw new ArgumentException(nameof(keys));
        }

        public Type Type { get; }

        public PropertyInfo PrimaryKeyProperty { get; }

        public TypeAccessor Accessor { get; }

        public IEnumerable<IKey> Keys { get; }

        public PrimaryKey PrimaryKey => Keys.OfType<PrimaryKey>().FirstOrDefault();

        public IEnumerable<ForeignKey> ForeignKeys => Keys.OfType<ForeignKey>().Where(x => x.Type == Type);

        public IEnumerable<ForeignKey> ChildKeys => Keys.OfType<ForeignKey>().Where(x => x.ParentType == Type);
    }
}
