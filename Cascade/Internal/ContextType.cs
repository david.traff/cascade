﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Internal
{
    public enum ContextType
    {
        None,
        Update,
        Delete
    }
}
