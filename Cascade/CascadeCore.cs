﻿using Cascade.Configuration;
using Cascade.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("Cascade.Tests")]
namespace Cascade
{
    public class CascadeCore
    {
        #region Register
        private static CascadeCore _Instance;
        private static readonly object _Lock = new object();

        public static CascadeCore Instance
        {
            get
            {
                if (_Instance == null)
                    throw new InvalidOperationException($"You need to build the {nameof(CascadeCore)} instance and register it with {nameof(CascadeCoreBuilder)}.");

                return _Instance;
            }
        }

        /// <summary>
        /// Useful for testing.
        /// </summary>
        internal static void Reset()
        {
            _Instance = null;
        }

        internal static void Register(ICascadeConfiguration config)
        {
            lock (_Lock)
            {
                if (_Instance != null)
                    throw new InvalidOperationException($"You may only register {nameof(CascadeCore)} once during runtime.");

                _Instance = new CascadeCore();
                _Instance.Init(config);
            }
        }
        #endregion

        private readonly Dictionary<Type, CascadeAttributes> _AttributeCache = new Dictionary<Type, CascadeAttributes>();
        private readonly Dictionary<Type, IEnumerable<ForeignKey>> _ForeignKeyCache = new Dictionary<Type, IEnumerable<ForeignKey>>();
        private bool _HasStore = false;

        public IEnumerable<CascadeAttributes> Attributes => _AttributeCache.Values;
        internal IEnumerable<ForeignKey> ForeignKeys => _AttributeCache.SelectMany(x => x.Value.ForeignKeys);

        private EventFactory _EventHandler;
        public EventFactory EventHandler
        {
            get
            {
                if (!_HasStore)
                    throw new InvalidOperationException($"You need to supply a {nameof(IDataStore)} when building.");

                return _EventHandler;
            }
        }

        private CascadeCore()
        {
            
        }

        private void Init(ICascadeConfiguration config)
        {
            InitAttributeCache(config);
            _HasStore = config.Store != null;

            _EventHandler = new EventFactory(config.EntityEventHandler);
        }

        private void InitAttributeCache(ICascadeConfiguration config)
        {
            var types = config.Types ?? Util.ScanCascadables();
            var relationFactory = config.Store == null ? new StorelessKeyFactory(config.PrimaryKeyMapper, types) : new KeyFactory(config.Store, config.PrimaryKeyMapper, types);
            var keys = relationFactory.CreateKeys().ToArray();

            foreach (var type in types)
            {
                var primary = config.PrimaryKeyMapper.GetPrimaryKey(type);

                var attributes = new CascadeAttributes(
                    type,
                    primary,
                    keys.Where(x => x.Type == type || (x is ForeignKey foreign && foreign.ParentType == type))
                );

                _AttributeCache.Add(type, attributes);
            }

            // Pass to check if all the related items are cached
            foreach (var kvp in _AttributeCache)
            {
                var attribute = kvp.Value;

                var missing = attribute.Keys.OfType<ForeignKey>().FirstOrDefault(x => !_AttributeCache.ContainsKey(x.ParentType));
                if (missing != null)
                    throw new InvalidOperationException($"You need to supply {nameof(ICascadable)} to the type {missing.ParentType.FullName}");
            }

            foreach(var kvp in _AttributeCache)
            {
                var children = ForeignKeys.Where(x => x.ParentType == kvp.Key);

                _ForeignKeyCache.Add(kvp.Key, children.ToArray());
            }
        }

        public bool TryGetAttributes(Type type, out CascadeAttributes attributes) => _AttributeCache.TryGetValue(type, out attributes);
    }
}
