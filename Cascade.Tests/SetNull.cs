﻿using Cascade.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade.Tests
{
    public class SetNull
    {
        [SetUp]
        public void Setup()
        {
            CascadeCore.Reset();
            _Store = Store.CreateWithData();

            CascadeCoreBuilder
                .Create()
                .WithTypes(typeof(Parent), typeof(SetNullChild))
                .WithStore(_Store)
                .Register();
        }

        private Store _Store;

        [Test]
        public void DeleteEntity()
        {
            //ON DELETE SET NULL: if a row of the referenced table is deleted,
            //then all referencing columns in all matching rows of the referencing table to be set to NULL.
            var parent = _Store.Parents.FirstOrDefault(x => x.Id == 1);

            Assert.NotNull(parent);
            Assert.AreEqual(2, _Store.SetNull.Count(x => x.ParentId == 1));
            Assert.AreEqual(0, _Store.SetNull.Count(x => x.ParentId == default));

            var result = CascadeCore.Instance.EventHandler.DeleteEntity(parent);

            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(0, _Store.Parents.Count(x => x.Id == parent.Id));
            Assert.AreEqual(0, _Store.SetNull.Count(x => x.ParentId == 1));
            Assert.AreEqual(2, _Store.SetNull.Count(x => x.ParentId == default));
        }

        [Test]
        public void UpdateEntity()
        {
            //ON UPDATE SET NULL: any change to a referenced column in the referenced table
            //causes the corresponding referencing column in matching rows of the referencing table to be set to NULL.
            //All the columns that make up the foreign key are set to NULL.
            var parent = _Store.Parents.FirstOrDefault(x => x.Id == 1);
            var children = _Store.SetNull.Where(x => x.ParentId == 1).ToArray();

            Assert.NotNull(parent);
            Assert.AreEqual(2, children.Length);

            CascadeCore.Instance.EventHandler.UpdateEntity(parent);

            Assert.AreEqual(0, _Store.SetNull.Count(x => x.ParentId == 1));
            Assert.AreEqual(0, children[0].ParentId);
            Assert.AreEqual(0, children[1].ParentId);
        }

        [Test]
        public void IsRestricted()
        {
            _Store.Parents.Add(new Parent { Id = 1234 });
            var parent = _Store.Parents.FirstOrDefault(x => x.Id == 1);

            Assert.NotNull(parent);
            Assert.AreEqual(2, _Store.SetNull.Count(x => x.ParentId == 1));

            //Parent shouldn't be restricted here since it's going to set child to null anyway.
            Assert.IsTrue(CascadeCore.Instance.EventHandler.CheckIntegrity(parent).Success);
            Assert.IsTrue(CascadeCore.Instance.EventHandler.CheckIntegrity(new Parent { Id = 1234 }).Success);
            Assert.IsTrue(CascadeCore.Instance.EventHandler.CheckIntegrity(new Parent { Id = 12345 }).Success);
        }
    }
}
