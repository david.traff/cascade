﻿using Cascade.Configuration;
using Cascade.Internal;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade.Tests
{
    public class Restrict
    {
        [SetUp]
        public void Setup()
        {
            CascadeCore.Reset();
            _Store = Store.CreateWithData();

            CascadeCoreBuilder
                .Create()
                .WithTypes(typeof(Parent), typeof(RestrictChild))
                .WithStore(_Store)
                .Register();
        }

        private Store _Store;

        [Test]
        public void DeleteEntity()
        {
            //ON DELETE RESTRICT: it is prohibited to delete a row of the referenced table
            //if that row has any matching rows in the referencing table.
            var parent = _Store.Parents.FirstOrDefault(x => x.Id == 1);

            Assert.NotNull(parent);
            Assert.AreEqual(2, _Store.Restrict.Count(x => x.ParentId == 1));
            Assert.AreEqual(0, _Store.Restrict.Count(x => x.ParentId == 1234));

            Assert.Throws(typeof(EntityRestrictedException), () => CascadeCore.Instance.EventHandler.DeleteEntity(parent));
            Assert.DoesNotThrow(() => CascadeCore.Instance.EventHandler.DeleteEntity(new Parent() { Id = 1234 }));
        }

        [Test]
        public void UpdateEntity()
        {
            //ON UPDATE RESTRICT: any change to a referenced column in the
            //referenced table is prohibited if there is a matching row.
            _Store.Restrict.Add(new RestrictChild { Id = 1234, ParentId = 1234 });
            var child = _Store.Restrict.FirstOrDefault(x => x.ParentId == 1);

            Assert.NotNull(child);
            Assert.Null(_Store.Parents.FirstOrDefault(x => x.Id == 1234));

            Assert.Throws(typeof(EntityRestrictedException), () => CascadeCore.Instance.EventHandler.UpdateEntity(child));
            Assert.Throws(typeof(MissingParentException), () => CascadeCore.Instance.EventHandler.UpdateEntity(new RestrictChild { Id = 1234, ParentId = 1234 }));
        }

        [Test]
        public void IsRestricted()
        {
            _Store.Parents.Add(new Parent { Id = 1234 });
            var parent = _Store.Parents.FirstOrDefault(x => x.Id == 1);

            Assert.NotNull(parent);
            Assert.AreEqual(2, _Store.Children.Count(x => x.ParentId == 1));

            Assert.IsFalse(CascadeCore.Instance.EventHandler.CheckIntegrity(parent).Success);
            Assert.IsTrue(CascadeCore.Instance.EventHandler.CheckIntegrity(new Parent { Id = 1234 }).Success);
            Assert.IsTrue(CascadeCore.Instance.EventHandler.CheckIntegrity(new Parent { Id = 12345 }).Success);
        }
    }
}
