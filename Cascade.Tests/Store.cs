﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Tests
{
    public class Store : IDataStore
    {
        public static Store CreateWithData()
        {
            var store = new Store()
            {
                Parents = new HashSet<Parent>(),
                Children = new HashSet<Child>(),
                Restrict = new HashSet<RestrictChild>(),
                SetNull = new HashSet<SetNullChild>(),
                GrandChildren = new HashSet<GrandChild>()
            };


            store.Parents.Add(new Parent() { Id = 1, Other = 0 });
            store.Parents.Add(new Parent() { Id = 2, Other = 0 });
            store.Parents.Add(new Parent() { Id = 3, Other = 0 });
            store.Parents.Add(new Parent() { Id = 4, Other = 0 });
            store.Parents.Add(new Parent() { Id = 5, Other = 0 });

            store.Parents.Add(new Parent() { Id = 6, Other = 0 });
            store.Parents.Add(new Parent() { Id = 7, Other = 0 });
            store.Parents.Add(new Parent() { Id = 8, Other = 0 });
            store.Parents.Add(new Parent() { Id = 9, Other = 0 });
            store.Parents.Add(new Parent() { Id = 10, Other = 0 });


            store.Children.Add(new Child() { Id = 1, ParentId = 1 });
            store.Children.Add(new Child() { Id = 2, ParentId = 1 });
            store.Children.Add(new Child() { Id = 3, ParentId = 2 });
            store.Children.Add(new Child() { Id = 4, ParentId = 2 });
            store.Children.Add(new Child() { Id = 5, ParentId = 3 });
            store.Children.Add(new Child() { Id = 6, ParentId = 3 });
            store.Children.Add(new Child() { Id = 7, ParentId = 4 });
            store.Children.Add(new Child() { Id = 8, ParentId = 4 });
            store.Children.Add(new Child() { Id = 9, ParentId = 5 });
            store.Children.Add(new Child() { Id = 10, ParentId = 5 });
            store.Children.Add(new Child() { Id = 11, ParentId = 6 });
            store.Children.Add(new Child() { Id = 12, ParentId = 6 });
            store.Children.Add(new Child() { Id = 13, ParentId = 7 });
            store.Children.Add(new Child() { Id = 14, ParentId = 7 });
            store.Children.Add(new Child() { Id = 15, ParentId = 8 });
            store.Children.Add(new Child() { Id = 16, ParentId = 8 });
            store.Children.Add(new Child() { Id = 17, ParentId = 9 });
            store.Children.Add(new Child() { Id = 18, ParentId = 9 });
            store.Children.Add(new Child() { Id = 19, ParentId = 10 });
            store.Children.Add(new Child() { Id = 20, ParentId = 10 });


            store.Restrict.Add(new RestrictChild() { Id = 1, ParentId = 1 });
            store.Restrict.Add(new RestrictChild() { Id = 2, ParentId = 1 });
            store.Restrict.Add(new RestrictChild() { Id = 3, ParentId = 2 });
            store.Restrict.Add(new RestrictChild() { Id = 4, ParentId = 2 });
            store.Restrict.Add(new RestrictChild() { Id = 5, ParentId = 3 });
            store.Restrict.Add(new RestrictChild() { Id = 6, ParentId = 3 });
            store.Restrict.Add(new RestrictChild() { Id = 7, ParentId = 4 });
            store.Restrict.Add(new RestrictChild() { Id = 8, ParentId = 4 });
            store.Restrict.Add(new RestrictChild() { Id = 9, ParentId = 5 });
            store.Restrict.Add(new RestrictChild() { Id = 10, ParentId = 5 });
            store.Restrict.Add(new RestrictChild() { Id = 11, ParentId = 6 });
            store.Restrict.Add(new RestrictChild() { Id = 12, ParentId = 6 });
            store.Restrict.Add(new RestrictChild() { Id = 13, ParentId = 7 });
            store.Restrict.Add(new RestrictChild() { Id = 14, ParentId = 7 });
            store.Restrict.Add(new RestrictChild() { Id = 15, ParentId = 8 });
            store.Restrict.Add(new RestrictChild() { Id = 16, ParentId = 8 });
            store.Restrict.Add(new RestrictChild() { Id = 17, ParentId = 9 });
            store.Restrict.Add(new RestrictChild() { Id = 18, ParentId = 9 });
            store.Restrict.Add(new RestrictChild() { Id = 19, ParentId = 10 });
            store.Restrict.Add(new RestrictChild() { Id = 20, ParentId = 10 });


            store.SetNull.Add(new SetNullChild() { Id = 1, ParentId = 1 });
            store.SetNull.Add(new SetNullChild() { Id = 2, ParentId = 1 });
            store.SetNull.Add(new SetNullChild() { Id = 3, ParentId = 2 });
            store.SetNull.Add(new SetNullChild() { Id = 4, ParentId = 2 });
            store.SetNull.Add(new SetNullChild() { Id = 5, ParentId = 3 });
            store.SetNull.Add(new SetNullChild() { Id = 6, ParentId = 3 });
            store.SetNull.Add(new SetNullChild() { Id = 7, ParentId = 4 });
            store.SetNull.Add(new SetNullChild() { Id = 8, ParentId = 4 });
            store.SetNull.Add(new SetNullChild() { Id = 9, ParentId = 5 });
            store.SetNull.Add(new SetNullChild() { Id = 10, ParentId = 5 });
            store.SetNull.Add(new SetNullChild() { Id = 11, ParentId = 6 });
            store.SetNull.Add(new SetNullChild() { Id = 12, ParentId = 6 });
            store.SetNull.Add(new SetNullChild() { Id = 13, ParentId = 7 });
            store.SetNull.Add(new SetNullChild() { Id = 14, ParentId = 7 });
            store.SetNull.Add(new SetNullChild() { Id = 15, ParentId = 8 });
            store.SetNull.Add(new SetNullChild() { Id = 16, ParentId = 8 });
            store.SetNull.Add(new SetNullChild() { Id = 17, ParentId = 9 });
            store.SetNull.Add(new SetNullChild() { Id = 18, ParentId = 9 });
            store.SetNull.Add(new SetNullChild() { Id = 19, ParentId = 10 });
            store.SetNull.Add(new SetNullChild() { Id = 20, ParentId = 10 });

            return store;
        }

        public HashSet<Parent> Parents { get; set; }

        public HashSet<Child> Children { get; set; }

        public HashSet<RestrictChild> Restrict { get; set; }

        public HashSet<SetNullChild> SetNull { get; set; }

        public HashSet<GrandChild> GrandChildren { get; set; }
    }
}
