﻿using Cascade.Configuration;
using Cascade.Internal;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Cascade.Tests
{
    public class Cascade
    {
        [SetUp]
        public void Setup()
        {
            CascadeCore.Reset();
            _Store = Store.CreateWithData();

            CascadeCoreBuilder
                .Create()
                .WithTypes(typeof(Parent), typeof(Child), typeof(GrandChild))
                .WithStore(_Store)
                .Register();
        }

        private Store _Store;

        [Test]
        public void DeleteEntity()
        {
            _Store.GrandChildren.Add(new GrandChild { Id = 1, ParentId = 0, OtherParentId = 1 });
            _Store.GrandChildren.Add(new GrandChild { Id = 2, ParentId = 0, OtherParentId = 1 });
            _Store.GrandChildren.Add(new GrandChild { Id = 3, ParentId = 0, OtherParentId = 2 });
            _Store.GrandChildren.Add(new GrandChild { Id = 4, ParentId = 0, OtherParentId = 2 });

            //ON DELETE CASCADE: if a row of the referenced table is deleted, then all matching rows in the referencing table are deleted.
            var parent = _Store.Parents.FirstOrDefault(x => x.Id == 1);

            Assert.NotNull(parent);
            Assert.AreEqual(2, _Store.Children.Count(x => x.ParentId == 1));
            Assert.AreEqual(2, _Store.GrandChildren.Count(x => x.OtherParentId == 1));
            Assert.AreEqual(2, _Store.GrandChildren.Count(x => x.OtherParentId == 2));

            var result = CascadeCore.Instance.EventHandler.DeleteEntity(parent);

            Assert.AreEqual(7, result.Count);
            Assert.AreEqual(0, _Store.Parents.Count(x => x.Id == parent.Id));
            Assert.AreEqual(0, _Store.Children.Count(x => x.ParentId == 1));
            Assert.AreEqual(0, _Store.GrandChildren.Count(x => x.OtherParentId == 1));
            Assert.AreEqual(0, _Store.GrandChildren.Count(x => x.OtherParentId == 2));
        }

        [Test]
        public void UpdateEntity()
        {
            //ON UPDATE CASCADE: any change to a referenced column in the referenced table causes
            //the same change to the corresponding referencing column in matching rows of the referencing table.
            _Store.Children.Add(new Child { Id = 1234, ParentId = 1234 });
            var child = _Store.Children.FirstOrDefault(x => x.ParentId == 1);

            Assert.NotNull(child);
            Assert.Null(_Store.Parents.FirstOrDefault(x => x.Id == 1234));

            Assert.DoesNotThrow(() => CascadeCore.Instance.EventHandler.UpdateEntity(child));
            Assert.Throws(typeof(MissingParentException), () => CascadeCore.Instance.EventHandler.UpdateEntity(new Child { Id = 1234, ParentId = 1234 }));
        }

        [Test]
        public void IsRestricted()
        {
            _Store.Parents.Add(new Parent { Id = 1234 });
            var parent = _Store.Parents.FirstOrDefault(x => x.Id == 1);

            Assert.NotNull(parent);
            Assert.AreEqual(2, _Store.Children.Count(x => x.ParentId == 1));

            Assert.IsTrue(CascadeCore.Instance.EventHandler.CheckIntegrity(parent).Success);
            Assert.IsTrue(CascadeCore.Instance.EventHandler.CheckIntegrity(new Parent { Id = 1234 }).Success);
            Assert.IsTrue(CascadeCore.Instance.EventHandler.CheckIntegrity(new Parent { Id = 12345 }).Success);
        }
    }
}
