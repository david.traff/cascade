﻿using Cascade.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Tests
{
    public class Child : ICascadable
    {
        [PrimaryKey]
        public int Id { get; set; }

        [ForeignKey(typeof(Parent), delete: CascadeMode.Cascade)]
        public int ParentId { get; set; }

        public int Other { get; set; }
    }

    public class GrandChild : ICascadable
    {
        [PrimaryKey]
        public int Id { get; set; }

        [ForeignKey(typeof(Child), delete: CascadeMode.Restrict)]
        public int ParentId { get; set; }

        [ForeignKey(typeof(Child), delete: CascadeMode.Cascade)]
        public int OtherParentId { get; set; }
    }

    public class RestrictChild : ICascadable
    {
        [PrimaryKey]
        public int Id { get; set; }

        [ForeignKey(typeof(Parent), delete: CascadeMode.Restrict, update: CascadeMode.Restrict)]
        public int ParentId { get; set; }

        public int Other { get; set; }
    }

    public class SetNullChild : ICascadable
    {
        [PrimaryKey]
        public int Id { get; set; }

        [ForeignKey(typeof(Parent), delete: CascadeMode.SetNull, update: CascadeMode.SetNull)]
        public int ParentId { get; set; }

        public int Other { get; set; }
    }

}
