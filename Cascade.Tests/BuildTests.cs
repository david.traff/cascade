﻿using Cascade.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade.Tests
{
    public class BuildTests
    {
        [SetUp]
        public void Setup()
        {
            CascadeCore.Reset();
        }

        [Test]
        public void Register()
        {
            Assert.Throws(typeof(InvalidOperationException), () => { var instance = CascadeCore.Instance; });

            CascadeCoreBuilder
                .Create()
                .Register();

            Assert.DoesNotThrow(() => { var instance = CascadeCore.Instance; });
        }

        [Test]
        public void RegisterScanner()
        {
            var parent = typeof(Parent);
            var parentId = typeof(Parent).GetProperty(nameof(Parent.Id));

            var child = typeof(Child);
            var childId = typeof(Child).GetProperty(nameof(Child.Id));
            var childParentId = typeof(Child).GetProperty(nameof(Child.ParentId));

            CascadeCoreBuilder
                .Create()
                .Register();

            Assert.AreEqual(5, CascadeCore.Instance.ForeignKeys.Count());
            Assert.AreEqual(5, CascadeCore.Instance.Attributes.Count());

            Assert.IsTrue(CascadeCore.Instance.Attributes.Any(x => x.Type == parent && x.PrimaryKeyProperty == parentId && x.ForeignKeys.Count() == 0));
            Assert.IsTrue(CascadeCore.Instance.Attributes.Any(x => x.Type == child && x.PrimaryKeyProperty == childId && x.ForeignKeys.Count() == 1));

            Assert.IsTrue(CascadeCore.Instance.Attributes.Any(x => x.Type == parent && x.PrimaryKeyProperty == parentId && x.ChildKeys.Count() == 3));
            Assert.IsTrue(CascadeCore.Instance.Attributes.Any(x => x.Type == child && x.PrimaryKeyProperty == childId && x.ChildKeys.Count() == 2));

            var key = CascadeCore.Instance.ForeignKeys.FirstOrDefault();

            Assert.AreEqual(typeof(Parent), key.ParentType);
            Assert.AreEqual(typeof(Child), key.Type);

            Assert.AreEqual(typeof(Parent).GetProperty(nameof(Parent.Id)), key.ParentProperty);
            Assert.AreEqual(typeof(Child).GetProperty(nameof(Child.ParentId)), key.Property);
        }

        [Test]
        public void RegisterManual()
        {
            CascadeCoreBuilder
                .Create()
                .WithType<Parent>()
                .Register();

            Assert.AreEqual(0, CascadeCore.Instance.ForeignKeys.Count());
            Assert.AreEqual(1, CascadeCore.Instance.Attributes.Count());
        }

        [Test]
        public void RegisterInvalidStore()
        {
            CascadeCoreBuilder
                .Create()
                .Register();

            Assert.Throws(typeof(InvalidOperationException), () => { var instance = CascadeCore.Instance.EventHandler; });
        }

        [Test]
        public void RegisterValidStore()
        {
            CascadeCoreBuilder
                .Create()
                .WithStore(Store.CreateWithData())
                .Register();

            Assert.DoesNotThrow(() => { var instance = CascadeCore.Instance.EventHandler; });
        }
    }
}
