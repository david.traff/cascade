﻿using Cascade.Configuration;
using Cascade.SQL;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Tests
{
    public class SQL
    {
        [SetUp]
        public void Setup()
        {
            CascadeCore.Reset();

            CascadeCoreBuilder
                .Create()
                .WithTypes(typeof(Parent), typeof(Child))
                .WithStore(Store.CreateWithData())
                .Register();
        }

        [Test]
        public void MySQL()
        {
            var generator = new MySQLKeyGenerator();
            var result = generator.GenerateSQL();

            Assert.AreEqual(
                "ALTER TABLE child\r\nADD CONSTRAINT FK_CHILD_PARENTID_PARENT_ID\r\nFOREIGN KEY (parentid) REFERENCES parent (id)\r\nON DELETE CASCADE\r\nON UPDATE NO ACTION;\r\n\r\n",
                result
            );
        }

        [Test]
        public void MySQLIfNotExists()
        {
            var generator = new MySQLKeyGenerator();
            var result = generator.GenerateSQL(new MySQLGenerationOptions() { IncludeIfNotExists = true });

            Assert.AreEqual(
                "IF NOT EXISTS (\r\n   SELECT NULL\r\n   FROM information_schema.TABLE_CONSTRAINTS\r\n   WHERE\r\n   CONSTRAINT_SCHEMA = DATABASE() AND\r\n   CONSTRAINT_NAME   = 'FK_CHILD_PARENTID_PARENT_ID' AND\r\n   CONSTRAINT_TYPE   = 'FOREIGN KEY'\r\n)\r\nTHEN\r\n   ALTER TABLE child\r\n   ADD CONSTRAINT FK_CHILD_PARENTID_PARENT_ID\r\n   FOREIGN KEY (parentid)    REFERENCES parent    (id)\r\n   ON DELETE CASCADE\r\n   ON UPDATE NO ACTION;\r\nEND IF;\r\n\r\n",
                result
            );
        }

        [Test]
        public void MySQLProcedure()
        {
            var generator = new MySQLKeyGenerator();
            var result = generator.GenerateSQL(new MySQLGenerationOptions() { StoredProcedure = true });

            Assert.AreEqual(
                "DELIMITER //\r\nCREATE PROCEDURE PROC_CHILD()\r\nBEGIN\r\nIF NOT EXISTS (\r\n      SELECT NULL\r\n      FROM information_schema.TABLE_CONSTRAINTS\r\n      WHERE\r\n      CONSTRAINT_SCHEMA = DATABASE() AND\r\n      CONSTRAINT_NAME   = 'FK_CHILD_PARENTID_PARENT_ID' AND\r\n      CONSTRAINT_TYPE   = 'FOREIGN KEY'\r\n)\r\nTHEN\r\n      ALTER TABLE child\r\n      ADD CONSTRAINT FK_CHILD_PARENTID_PARENT_ID\r\n      FOREIGN KEY (parentid)       REFERENCES parent       (id)\r\n      ON DELETE CASCADE\r\n      ON UPDATE NO ACTION;\r\nEND IF;\r\n\r\nEND //\r\nDELIMITER ; \r\n",
                result
            );
        }
    }
}
