﻿using Cascade.Internal;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade.Tests
{
    public class CollectionWrapperTests
    {
        [Test]
        public void RemoveWhere()
        {
            var data = new HashSet<Parent>()
            {
                new Parent() { Id = 1 },
                new Parent() { Id = 2 },
                new Parent() { Id = 3 },
                new Parent() { Id = 4 },
                new Parent() { Id = 5 },
                new Parent() { Id = 6 },
            };

            var wrapper = new CollectionWrapper<Parent>(data, typeof(Parent).GetProperty(nameof(Parent.Id)));
            var removed = wrapper.RemoveWhere(1);

            Assert.AreEqual(1, removed.Count());
            Assert.AreEqual(5, data.Count);

            data = new HashSet<Parent>()
            {
                new Parent() { Id = 1, Other = 0 },
                new Parent() { Id = 2, Other = 0 },
                new Parent() { Id = 3, Other = 0 },
                new Parent() { Id = 4, Other = 1 },
                new Parent() { Id = 5, Other = 1 },
                new Parent() { Id = 6, Other = 1 },
            };

            wrapper = new CollectionWrapper<Parent>(data, typeof(Parent).GetProperty(nameof(Parent.Other)));
            removed = wrapper.RemoveWhere(1);

            Assert.AreEqual(3, removed.Count());
            Assert.AreEqual(3, data.Count);
        }

        [Test]
        public void InvalidParameters()
        {
            Assert.Throws(typeof(ArgumentException), () => new CollectionWrapper(3, null));
        }

        [Test]
        public void SetNull()
        {
            var data = new HashSet<Parent>()
            {
                new Parent() { Id = 1 },
                new Parent() { Id = 2 },
                new Parent() { Id = 3 },
                new Parent() { Id = 4 },
                new Parent() { Id = 5 },
                new Parent() { Id = 6 },
            };

            var wrapper = new CollectionWrapper<Parent>(data, typeof(Parent).GetProperty(nameof(Parent.Id)));
            var result = wrapper.SetNull(1);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(6, data.Count);
            Assert.AreEqual(0, data.First().Id);

            data = new HashSet<Parent>()
            {
                new Parent() { Id = 1, Other = 0 },
                new Parent() { Id = 2, Other = 0 },
                new Parent() { Id = 3, Other = 0 },
                new Parent() { Id = 4, Other = 1 },
                new Parent() { Id = 5, Other = 1 },
                new Parent() { Id = 6, Other = 1 },
            };

            wrapper = new CollectionWrapper<Parent>(data, typeof(Parent).GetProperty(nameof(Parent.Other)));
            result = wrapper.SetNull(1);

            Assert.AreEqual(3, result.Count());
            Assert.AreEqual(6, data.Count);
            Assert.AreEqual(0, data.ElementAt(3).Other);
            Assert.AreEqual(0, data.ElementAt(4).Other);
            Assert.AreEqual(0, data.ElementAt(5).Other);
        }

        [Test]
        public void Restrict()
        {
            var data = new HashSet<Parent>()
            {
                new Parent() { Id = 1 },
                new Parent() { Id = 2 },
                new Parent() { Id = 3 },
                new Parent() { Id = 4 },
                new Parent() { Id = 5 },
                new Parent() { Id = 6 },
            };

            var wrapper = new CollectionWrapper<Parent>(data, typeof(Parent).GetProperty(nameof(Parent.Id)));
            var restrict = wrapper.Restrict(1);

            Assert.IsNotNull(restrict);
            Assert.AreEqual(6, data.Count);

            data = new HashSet<Parent>()
            {
                new Parent() { Id = 1, Other = 0 },
                new Parent() { Id = 2, Other = 0 },
                new Parent() { Id = 3, Other = 0 },
                new Parent() { Id = 4, Other = 1 },
                new Parent() { Id = 5, Other = 1 },
                new Parent() { Id = 6, Other = 1 },
            };

            wrapper = new CollectionWrapper<Parent>(data, typeof(Parent).GetProperty(nameof(Parent.Other)));
            restrict = wrapper.Restrict(0);

            Assert.IsNotNull(restrict);
            Assert.AreEqual(6, data.Count);

            restrict = wrapper.Restrict(2);

            Assert.IsNull(restrict);
        }

        [Test]
        public void Ctor()
        {
            Assert.Throws(typeof(ArgumentException), () => new CollectionWrapper<Parent>(null, typeof(Parent).GetProperty(nameof(Parent.Id))));

            Assert.Throws(typeof(ArgumentException), () => new CollectionWrapper<Parent>(new HashSet<Parent>(), null));

            Assert.Throws(typeof(ArgumentException), () => new CollectionWrapper<Parent>(new HashSet<Parent>(), typeof(Child).GetProperty(nameof(Child.Id))));
        }
    }
}
