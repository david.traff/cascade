﻿using Cascade.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.Tests
{
    public class Parent : ICascadable
    {
        [PrimaryKey]
        public int Id { get; set; }

        public int Other { get; set; }
    }
}
