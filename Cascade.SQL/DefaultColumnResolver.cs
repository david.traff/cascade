﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Cascade.SQL
{
    public class DefaultColumnResolver : IColumnResolver
    {
        public string Resolve(PropertyInfo property)
        {
            return property.Name.ToLowerInvariant();
        }
    }
}
