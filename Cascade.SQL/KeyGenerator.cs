﻿using Cascade.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade.SQL
{
    public abstract class KeyGenerator<TGenerationOptions> where TGenerationOptions : IGenerationOptions
    {
        public KeyGenerator(ITableResolver tableResolver, IColumnResolver columnResolver)
        {
            TableResolver = tableResolver ?? throw new ArgumentException(nameof(tableResolver));
            ColumnResolver = columnResolver ?? throw new ArgumentException(nameof(columnResolver));
        }

        public KeyGenerator()
        {
            TableResolver = new DefaultTableResolver();
            ColumnResolver = new DefaultColumnResolver();
        }

        protected ITableResolver TableResolver { get; }

        protected IColumnResolver ColumnResolver { get; }

        public string GenerateSQL(TGenerationOptions options, params Type[] types)
        {
            if (options == null)
                throw new ArgumentException(nameof(options));

            var sb = new StringBuilder();
            foreach (var type in types)
                GenerateFromType(sb, type, options);

            return sb.ToString();
        }

        public string GenerateSQL(TGenerationOptions options)
        {
            return GenerateSQL(options, CascadeCore.Instance.Attributes.Select(x => x.Type).ToArray());
        }

        public string GenerateSQL(params Type[] types)
        {
            var instance = Activator.CreateInstance<TGenerationOptions>();

            return GenerateSQL(instance, types);
        }

        public string GenerateSQL()
        {
            return GenerateSQL(CascadeCore.Instance.Attributes.Select(x => x.Type).ToArray());
        }

        private void GenerateFromType(StringBuilder sb, Type type, TGenerationOptions options)
        {
            if (!CascadeCore.Instance.TryGetAttributes(type, out var attributes))
                throw new ArgumentException($"{nameof(type)} is not registered in {nameof(CascadeCore)}");

            GenerateDDL(sb, attributes, options);
        }

        protected abstract void GenerateDDL(StringBuilder sb, CascadeAttributes attributes, TGenerationOptions options);
    }
}
