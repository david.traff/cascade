﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.SQL
{
    public interface IGenerationOptions
    {
        IGenerationOptions GetDefault();
    }
}
