﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.SQL
{
    public interface ITableResolver
    {
        string Resolve(Type type);
    }
}
