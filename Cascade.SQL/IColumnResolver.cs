﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Cascade.SQL
{
    public interface IColumnResolver
    {
        string Resolve(PropertyInfo property);
    }
}
