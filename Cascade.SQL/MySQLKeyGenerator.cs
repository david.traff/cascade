﻿using Cascade.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cascade.SQL
{
    public class MySQLKeyGenerator : KeyGenerator<MySQLGenerationOptions>
    {
        private static readonly Dictionary<CascadeMode, string> _ModeMap = new Dictionary<CascadeMode, string>
        {
            { CascadeMode.Cascade, "CASCADE" },
            { CascadeMode.NoAction, "NO ACTION" },
            { CascadeMode.Restrict, "RESTRICT" },
            { CascadeMode.SetNull, "SET NULL" }
        };

        public MySQLKeyGenerator(ITableResolver nameResolver, IColumnResolver columnResolver) : base(nameResolver, columnResolver)
        {
            
        }

        public MySQLKeyGenerator() : base()
        {

        }

        protected override void GenerateDDL(StringBuilder sb, CascadeAttributes attributes, MySQLGenerationOptions options)
        {
            var stored = options.StoredProcedure && attributes.ForeignKeys.Count() > 0;
            var include = stored || options.IncludeIfNotExists;
            var foreignLevel = stored ? 1 : 0;

            if (stored)
            {
                sb.AppendLine("DELIMITER //");
                sb.AppendLine($"CREATE PROCEDURE {ProcedureName(attributes)}()");
                sb.AppendLine("BEGIN");
            }

            foreach (var key in attributes.ForeignKeys)
                GenerateKey(sb, key, attributes, include, foreignLevel);

            if (stored)
            {
                sb.AppendLine("END //");
                sb.AppendLine("DELIMITER ; ");
            }
        }

        private void GenerateKey(StringBuilder sb, ForeignKey key, CascadeAttributes attributes, bool notExists, int indentLevel)
        {
            var name = ConstraintName(key, attributes);
            var indent = notExists ? Indent(indentLevel + 1): Indent(indentLevel);

            if (notExists)
            {
                sb.AppendLine("IF NOT EXISTS (");
                sb.Append(indent); sb.AppendLine("SELECT NULL");
                sb.Append(indent); sb.AppendLine("FROM information_schema.TABLE_CONSTRAINTS");
                sb.Append(indent); sb.AppendLine("WHERE");

                sb.Append(indent); sb.AppendLine("CONSTRAINT_SCHEMA = DATABASE() AND");
                sb.Append(indent); sb.AppendLine($"CONSTRAINT_NAME   = '{name}' AND");
                sb.Append(indent); sb.AppendLine("CONSTRAINT_TYPE   = 'FOREIGN KEY'");
                sb.AppendLine(")");
                sb.AppendLine("THEN");
            }

            sb.Append(indent); sb.AppendLine($"ALTER TABLE {TableResolver.Resolve(key.Type)}");
            sb.Append(indent); sb.AppendLine($"ADD CONSTRAINT {name}");

            sb.Append(indent); sb.Append($"FOREIGN KEY ({ColumnResolver.Resolve(key.Property)}) ");
            sb.Append(indent); sb.Append($"REFERENCES {TableResolver.Resolve(key.ParentType)} ");
            sb.Append(indent); sb.AppendLine($"({ColumnResolver.Resolve(key.ParentKey.Property)})");

            sb.Append(indent); sb.AppendLine($"ON DELETE {_ModeMap[key.OnDelete]}");
            sb.Append(indent); sb.AppendLine($"ON UPDATE {_ModeMap[key.OnUpdate]};");

            if (notExists)
                sb.AppendLine("END IF;");

            sb.AppendLine();
        }

        private string ConstraintName(ForeignKey key, CascadeAttributes attributes)
        {
            //FK_CHILDTABLE_CHILDCOLUMN_PARENTTABLE_PARENTPRIMARYKEYCOLUMN
            var childTableName = TableResolver.Resolve(attributes.Type);
            var childColumnName = ColumnResolver.Resolve(key.Property);

            var parentTableName = TableResolver.Resolve(key.ParentType);
            var parentColumnName = ColumnResolver.Resolve(key.ParentKey.Property);

            return $"FK_{childTableName}_{childColumnName}_{parentTableName}_{parentColumnName}".ToUpperInvariant();
        }

        private string ProcedureName(CascadeAttributes attributes)
        {
            return $"proc_{TableResolver.Resolve(attributes.Type)}".ToUpperInvariant();
        }

        private string Indent(int level) => new string(' ', level * 3);
    }
}
