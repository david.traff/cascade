﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.SQL
{
    public class MySQLGenerationOptions : IGenerationOptions
    {
        public bool IncludeIfNotExists { get; set; }

        public bool StoredProcedure { get; set; }

        public IGenerationOptions GetDefault()
        {
            return new MySQLGenerationOptions()
            {
                IncludeIfNotExists = false
            };
        }
    }
}
