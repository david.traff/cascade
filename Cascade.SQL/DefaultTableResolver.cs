﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascade.SQL
{
    public class DefaultTableResolver : ITableResolver
    {
        public string Resolve(Type type)
        {
            return type.Name.ToLowerInvariant();
        }
    }
}
